package com.example.pavlo.erthquakeapplication;

import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.pavlo.erthquakeapplication.weather.Temperature;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by pavlo on 22.03.2018.
 */

public class WeatherUpdateService extends Service {
    private Timer timer;

    @Nullable
    @Override
    public IBinder onBind(Intent intent){
        return null;
    }

    private void addNewWeather(Temperature temperature){
        ContentResolver contentResolver = getContentResolver();

        Log.i("WeatherUpdateService", "onBind");
            ContentValues values = new ContentValues();
            values.put(TemperatureContentProvider.KEY_DATE, temperature.getDate().toString());
            values.put(TemperatureContentProvider.KEY_FEELING_TEMPERATRURE, temperature.getFeelingTemperature());
            values.put(TemperatureContentProvider.KEY_LOW_TEMPERATRURE, temperature.getLowTemperature());
            values.put(TemperatureContentProvider.KEY_HIGH_TEMPERATRURE, temperature.getHighTemperature());
            contentResolver.insert(TemperatureContentProvider.CONTENT_URI, values);
    }

    public void refreshWeather(){
        URL url;
        Log.i("WeatherUpdateService", "refreshWeather");
        try {
            String wearher = getString(R.string.authoriazation_token);
            url = new URL(wearher);
            URLConnection urlConnection = url.openConnection();

            HttpURLConnection httpURLConnection = (HttpURLConnection) urlConnection;
            int responseCode = httpURLConnection.getResponseCode();

            if(responseCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                StringBuilder responseStrBuilder = new StringBuilder();

                String inputStr;
                while((inputStr = streamReader.readLine()) != null) {
                    responseStrBuilder.append(inputStr);
                }

                JSONObject obj = new JSONObject(responseStrBuilder.toString());
//                Date dateTime = new java.util.Date((long)obj.getInt("dt")*1000);

                JSONObject main = obj.getJSONObject("main");
                double low = main.getDouble("temp_min") - 273.6;
                double high = main.getDouble("temp_max") - 273.6;
                double feelslike = main.getDouble("temp") - 273.6;

                Temperature temperature = new Temperature(low, high, feelslike, Calendar.getInstance().getTime());
                addNewWeather(temperature);
            }
        } catch(MalformedURLException e) {
            e.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        } catch(JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        Context context = getApplicationContext();
        timer.cancel();
        if(true) {
            timer = new Timer("weatherUpdates");
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run(){
                    refreshWeather();
                }
            }, 0, 5 * 60 * 1000);
        } else {
            Thread t = new Thread(new Runnable() {
                public void run(){
                    refreshWeather();
                }
            });
            t.start();
        }
        return Service.START_STICKY;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        timer = new Timer("weatherUpdates");
    }
}

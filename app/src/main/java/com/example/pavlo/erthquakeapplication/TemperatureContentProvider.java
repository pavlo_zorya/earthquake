package com.example.pavlo.erthquakeapplication;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * Created by pavlo on 22.03.2018.
 */

public class TemperatureContentProvider extends ContentProvider {
    public static final Uri CONTENT_URI = Uri.parse("content://com.paad.weatherprovider/temperatures");
    public static final String KEY_ID = "_id";
    public static final String KEY_DATE = "date";
    public static final String KEY_LOW_TEMPERATRURE = "low_temperature";
    public static final String KEY_HIGH_TEMPERATRURE = "high_temperature";
    public static final String KEY_FEELING_TEMPERATRURE = "feeling_temperature";
    public static final String KEY_SUMMARY = "summary";
    private static final int TEMPERATURES = 1;
    private static final int TEMPERATURES_ID = 2;

    TemperatureDbHelper temperatureDbHelper;

    private static final UriMatcher uriMatcher;

    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI("com.paad.weatherprovider", "temperatures", TEMPERATURES);
        uriMatcher.addURI("com.paad.weatherprovider", "temperatures/#", TEMPERATURES_ID);
    }

    @Override
    public String getType(Uri uri){
        switch(uriMatcher.match(uri)) {
            case TEMPERATURES:
                return "vnd.android.cursor.dir/vnd.paad.temperature";
            case TEMPERATURES_ID:
                return "vnd.android.cursor.item/vnd.paad.temperature";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Override
    public boolean onCreate(){
        temperatureDbHelper = new TemperatureDbHelper(getContext(), TemperatureDbHelper.DATABASE_NAME, null, TemperatureDbHelper.DATABASE_VERSION);
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sort){
        SQLiteDatabase database = temperatureDbHelper.getWritableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(TemperatureDbHelper.TEMPERATURES_TABLE);
// Если это запрос строки, то ограничьте выборку переданной строкой,
        switch(uriMatcher.match(uri)) {
            case TEMPERATURES_ID:
                qb.appendWhere(KEY_ID + " = " + uri.getPathSegments().get(1));
                break;
            default:
                break;
        }

        String orderBy;
        if(TextUtils.isEmpty(sort)) {
            orderBy = KEY_DATE;
        } else {
            orderBy = sort;
        }
        Cursor c = qb.query(database, projection, selection, selectionArgs, null, null, orderBy);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri _uri, @Nullable ContentValues contentValues){
        SQLiteDatabase database = temperatureDbHelper.getWritableDatabase();
        long rowID = database.insert(TemperatureDbHelper.TEMPERATURES_TABLE, "temperature", contentValues);
        if(rowID > 0) {
            Uri uri = ContentUris.withAppendedId(CONTENT_URI, rowID);
            getContext().getContentResolver().notifyChange(uri, null);
            return uri;
        }
        throw new SQLException("Failed to insert row into " + _uri);

    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String where, @Nullable String[] whereArgs){

        SQLiteDatabase database = temperatureDbHelper.getWritableDatabase();
        int count;
        switch(uriMatcher.match(uri)) {
            case TEMPERATURES:
                count = database.delete(TemperatureDbHelper.TEMPERATURES_TABLE, where, whereArgs);
                break;
            case TEMPERATURES_ID:
                String segment = uri.getPathSegments().get(1);
                count = database.delete(TemperatureDbHelper.TEMPERATURES_TABLE, KEY_ID + "=" + segment + (!TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;

    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String where, @Nullable String[] whereArgs){
        int count;
        SQLiteDatabase database = temperatureDbHelper.getWritableDatabase();
        switch(uriMatcher.match(uri)) {
            case TEMPERATURES:
                count = database.update(TemperatureDbHelper.TEMPERATURES_TABLE, contentValues, where, whereArgs);
                break;
            case TEMPERATURES_ID:
                String segment = uri.getPathSegments().get(1);
                count = database.update(TemperatureDbHelper.TEMPERATURES_TABLE, contentValues, KEY_ID + "=" + segment + (TextUtils.isEmpty(where) ? " AND (" + where + ')' : ""), whereArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;

    }

    private static class TemperatureDbHelper extends SQLiteOpenHelper {
        private static final String TAG = "WeatherProvider";
        private static final String DATABASE_NAME = "weatherdb.db";
        private static final int DATABASE_VERSION = 1;
        private static final String TEMPERATURES_TABLE = "temperatures";

        private static final String DATABASE_CREATE = "create table "
                + TEMPERATURES_TABLE + " ("
                + KEY_ID + " integer primary key autoincrement, "
                + KEY_DATE + " INTEGER, "
                + KEY_FEELING_TEMPERATRURE + " KEY_FEELING_TEMPERATRURE, "
                + KEY_HIGH_TEMPERATRURE + " KEY_HIGH_TEMPERATRURE, "
                + KEY_LOW_TEMPERATRURE + " KEY_LOW_TEMPERATRURE);";

        private SQLiteOpenHelper temperatureDB;

        public TemperatureDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase){
            sqLiteDatabase.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1){
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TEMPERATURES_TABLE);
            onCreate(sqLiteDatabase);
        }
    }
}

package com.example.pavlo.erthquakeapplication.weather;

/**
 * Created by pavlo on 22.03.2018.
 */

public class Cloudiness {
    public Cloudiness(int percentage, int type){
        this.percentage = percentage;
        this.type = type;
    }

    private static final String CLOUDINESS = "cloudiness";
    public int getPercentage(){
        return percentage;
    }

    public void setPercentage(int percentage){
        this.percentage = percentage;
    }

    public int getType(){
        return type;
    }

    public void setType(int type){
        this.type = type;
    }

    private int percentage;
    private int type;
}

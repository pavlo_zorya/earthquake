package com.example.pavlo.erthquakeapplication.weather;

import android.location.Location;

import java.util.Date;

/**
 * Created by pavlo on 22.03.2018.
 */

public class Weather {
    private Date date;
    private String details;
    private Location location;
    private double humidity;
    private double pressure;
    private Cloudiness cloudiness;
    private Precipitation precipitation;
    private Temperature temperature;

}

package com.example.pavlo.erthquakeapplication.weather;

import java.util.Date;

/**
 * Created by pavlo on 22.03.2018.
 */

public class Temperature {
    private static final String TEMPERATURE  = "temperature";
    public Temperature(double lowTemperature, double highTemperature, double feelingTemperature, Date date){
        this.lowTemperature = lowTemperature;
        this.feelingTemperature = feelingTemperature;
        this.highTemperature = highTemperature;
        this.date = date;
    }

    public double getLowTemperature(){
        return lowTemperature;
    }

    public void setLowTemperature(double lowTemperature){
        this.lowTemperature = lowTemperature;
    }

    public double getFeelingTemperature(){
        return feelingTemperature;
    }

    public void setFeelingTemperature(double feelingTemperature){
        this.feelingTemperature = feelingTemperature;
    }

    public double getHighTemperature(){
        return highTemperature;
    }

    public void setHighTemperature(double highTemperature){
        this.highTemperature = highTemperature;
    }

    private double lowTemperature;
    private double feelingTemperature;
    private double highTemperature;

    public Date getDate(){
        return date;
    }

    public void setDate(Date date){
        this.date = date;
    }

    private Date date;
}

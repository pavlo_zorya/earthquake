package com.example.pavlo.erthquakeapplication.weather;

/**
 * Created by pavlo on 22.03.2018.
 */

public class Precipitation {
    public Precipitation(int type, double amount, int intensity){
        this.type = type;
        this.amount = amount;
        this.intensity = intensity;
    }

    public int getType(){

        return type;
    }

    public void setType(int type){
        this.type = type;
    }

    public double getAmount(){
        return amount;
    }

    public void setAmount(double amount){
        this.amount = amount;
    }

    public int getIntensity(){
        return intensity;
    }

    public void setIntensity(int intensity){
        this.intensity = intensity;
    }

    private static final String PRECIPITATION = "precipitation";
    private int type;
    private double amount;
    private int intensity;
}

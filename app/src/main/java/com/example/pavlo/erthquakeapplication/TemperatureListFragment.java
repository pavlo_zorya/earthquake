package com.example.pavlo.erthquakeapplication;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.widget.SimpleCursorAdapter;

import com.example.pavlo.erthquakeapplication.weather.Temperature;

import java.util.ArrayList;

/**
 * Created by pavlo on 22.03.2018.
 */

public class TemperatureListFragment extends ListFragment implements LoaderManager.LoaderCallbacks {
    SimpleCursorAdapter adapter;
    ArrayList<Temperature> weatherArrayList = new ArrayList<>();
    private Handler handler = new Handler();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);

        adapter = new SimpleCursorAdapter(getActivity(), R.layout.list_row_element, null,
                new String[]{TemperatureContentProvider.KEY_FEELING_TEMPERATRURE, TemperatureContentProvider.KEY_LOW_TEMPERATRURE, TemperatureContentProvider.KEY_DATE},
                new int[]{R.id.temp_min, R.id.temp_high, R.id.date}, 0);
        setListAdapter(adapter);

        getLoaderManager().initLoader(0, null, this);
        refreshWeather();
    }

    void refreshWeather(){
        getLoaderManager().restartLoader(0, null, TemperatureListFragment.this);
        getActivity().startService(new Intent(getActivity(), WeatherUpdateService.class));
    }


    @Override
    public Loader onCreateLoader(int i, Bundle bundle){
        String[] projection = new String[]{TemperatureContentProvider.KEY_ID,
                TemperatureContentProvider.KEY_DATE
                ,TemperatureContentProvider.KEY_HIGH_TEMPERATRURE,
                TemperatureContentProvider.KEY_LOW_TEMPERATRURE,
                TemperatureContentProvider.KEY_FEELING_TEMPERATRURE};
        MainActivity mainActivity = (MainActivity) getActivity();
        String where = TemperatureContentProvider.KEY_HIGH_TEMPERATRURE + " > -100";
        String sort = TemperatureContentProvider.KEY_DATE;
        CursorLoader loader = new CursorLoader(getActivity(), TemperatureContentProvider.CONTENT_URI, projection, where, null, sort);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader loader, Object o){
        adapter.swapCursor((Cursor) o);
    }

    @Override
    public void onLoaderReset(Loader loader){
        adapter.swapCursor(null);
    }
}